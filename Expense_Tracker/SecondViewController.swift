//
//  SecondViewController.swift
//  Expense_Tracker
//
//  Created by Bradley Roring on 10/15/17.
//  Copyright © 2017 Bradley Roring. All rights reserved.
//

import UIKit
import CoreData

class SecondViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate  {

    var expense: Expense?
    
    @IBOutlet var editPlace: UITextField!
    @IBOutlet var editCost: UITextField!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var editNotes: UITextView!
    
    let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
    var expenses: [Expense] = []
    var receivedData: Expense?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.editCost.delegate = self
        self.editPlace.delegate = self
        self.editNotes.delegate = self
        
        if receivedData != nil
        {
            let amountString = String(format: "%.2f", receivedData!.amount)
            editPlace.text = receivedData?.name
            //amountTxtField.text = String(describing: receivedData!.amount)
            editCost.text = amountString
            editNotes.text = receivedData?.notes
            datePicker.date = (receivedData?.theDate)!
        }
        else
        {
            editPlace.text = ""
            editCost.text = ""
            editNotes.text = ""
        }
        
        
        
        let saveButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(buttonTapped(_:)))
        
        /*let deleteButton = UIBarButtonItem(barButtonSystemItem:
            .trash, target: self, action:
            #selector(trashTapped(_:))) */
        
        self.navigationItem.rightBarButtonItems = [saveButton]
        // Do any additional setup after loading the view.
    }

    @objc func buttonTapped (_ sender : UIButton) {
        let data = Expense(context: context)
        
        data.name = editPlace.text
        data.amount = (editCost.text! as NSString).doubleValue
        data.notes = editNotes.text
        data.theDate = datePicker.date
        
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        let alertController = UIAlertController(title: "Saved", message:
            "", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
        editPlace.text = ""
        editCost.text = ""
        editNotes.text = ""
    }
    
    /*@objc func trashTapped (_ sender : UIButton) {
        
    }*/
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        editNotes.resignFirstResponder()
        editCost.resignFirstResponder()
        editNotes.resignFirstResponder()
        return (true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
